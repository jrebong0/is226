<?php

namespace Raw\Controllers;

use Raw\Core\BaseController;
use Raw\Core\Template;

class IndexController extends BaseController
{
    public function index()
    {
        header('Location: /home/');
    }
}
