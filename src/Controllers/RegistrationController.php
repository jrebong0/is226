<?php

namespace Raw\Controllers;

use Raw\Core\BaseController;
use Raw\Core\Template;
use Raw\Model\Employee;

class RegistrationController extends BaseController
{
    public function index()
    {
        $logged = $this->session->isLoggedIn();
        return Template::render('registration/index.html', compact('logged'));
    }

    public function edit()
    {
        $logged = $this->session->isLoggedIn();
        $employee = new Employee();
        $employee->getById($this->getUrlParam());
        return Template::render('registration/index.html', compact('logged', 'employee'));
    }

    public function delete()
    {
        $employee = new Employee();
        $employee->delete($this->getUrlParam());
        return Template::render('registration/deleted.html');
    }

    public function register()
    {
        $error = '';
        $id = $this->post('employeeid');
        $firstname = $this->post('firstname');
        $lastname = $this->post('lastname');
        $email = $this->post('email');

        $_POST['id'] = $_POST['employeeid'];
        $employee = new Employee($_POST);

        if ($id == '0') {
            $result = $employee->create();

            if ($result === true) {
                return Template::render('registration/thankyou.html', compact('firstname', 'lastname'));
            } else {
                $error = $result;
                return Template::render('registration/thankyou.html', compact('error', 'firstname', 'lastname'));
            }
        } else {
            $result = $employee->save();

            if ($result === true) {
                header('Location: /registration/edit/'. $id);
            } else {
                $error = $result;
                return Template::render('registration/thankyou.html', compact('error', 'firstname', 'lastname'));
            }
        }
    }
}
