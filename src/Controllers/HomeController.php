<?php

namespace Raw\Controllers;

use Raw\Core\BaseController;
use Raw\Core\Template;
use Raw\Model\Employee;
use Raw\Model\Employees;

class HomeController extends BaseController
{
    public function index()
    {
      $logged = $this->session->isLoggedIn();

      if ($logged) {
        $dbEmp = new Employees();
        $employees = $dbEmp->get();
        return Template::render('home/index.html', compact('logged', 'employees'));
      }
      return Template::render('home/index.html', compact('logged'));
    }

    public function login()
    {
        $email = $this->post('email');
        $password = $this->post('password');
        $error = false;

        if (!empty($email)) {
          $employee = new Employee([]);
          $result = $employee->login($email);

          if ($result === false || !password_verify($password, $employee->password))
          {
            $error = 'Invalid username or password';
            return Template::render('home/login.html', compact('error', 'email', 'password'));
          }

          $this->session->login($employee);
          header('Location: /home/');
        } else {
          return Template::render('home/login.html', compact('error', 'email', 'password'));
        }
    }

    public function logout()
    {
      $this->session->logout();
      header('Location: /home/');
    }
}
