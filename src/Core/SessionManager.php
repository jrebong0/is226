<?php

namespace Raw\Core;

class SessionManager
{
    public function __construct()
    {
      session_start();
    }

    public function login($user)
    {
      $_SESSION['logged_in_username'] = $user->email;
      $_SESSION['logged_in_firstname'] = $user->firstname;
      $_SESSION['logged_in_lastname'] = $user->lastname;
    }

    public function logout()
    {
      session_unset();
      session_destroy();
    }

    public function isLoggedIn()
    {
        return !empty($_SESSION) && !empty($_SESSION['logged_in_username']);
    }

    public function getUsername()
    {
        return $_SESSION['logged_in_username'];
    }

    public function getUserFullname()
    {
        return $_SESSION['logged_in_firstname'] + ' ' + $_SESSION['logged_in_lastname'];
    }
}
