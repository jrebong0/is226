<?php

namespace Raw\Core;
use Raw\Core\SessionManager;

class BaseController
{
    protected $session;

    public function __construct()
    {
        $this->session = new SessionManager();
    }

    public function get($index)
    {
        return $this->getFromArray($_GET, $index);
    }

    public function post($index)
    {
        return $this->getFromArray($_POST, $index);
    }

    public function getUrlParam() {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        return $uri_segments[count($uri_segments) - 1];
    }

    protected function getFromArray($array, $index)
    {
        if (!isset($index))
        {
            return null;
        }
        if (!isset($array))
        {
            return null;
        }
        if (!isset($array[$index]))
        {
            return null;
        }
        $value = $array[$index];

        return $value;
    }
}
