<?php

namespace Raw\Model;

use Raw\Model\BaseModel;
use \PASSWORD_DEFAULT;

class Employee extends BaseModel implements \JsonSerializable
{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $jobdescription;
    public $datehired;
    public $jobtype;
    public $jobstatus;
    public $password;
    public $created_at;

    public function __construct($data=[])
    {
        if (!empty($data)) {
            $this->populate($data);
            $this->password  = $this->hashPassword($data['password']);
        }
        parent::__construct();
    }

    public function create()
    {
        $statement = $this->db->prepare('INSERT INTO employee SET
                                        firstname=:firstname,
                                        lastname=:lastname,
                                        email=:email,
                                        jobdescription=:jobdescription,
                                        datehired=:datehired,
                                        jobtype=:jobtype,
                                        jobstatus=:jobstatus,
                                        password=:password');
        try {
            $result = $statement->execute([
                ':firstname' => $this->firstname,
                ':lastname'  => $this->lastname,
                ':email'     => $this->email,
                ':jobdescription' => $this->jobdescription,
                ':datehired' => $this->datehired,
                ':jobtype'   => $this->jobtype,
                ':jobstatus' => $this->jobstatus,
                ':password'  => $this->password
            ]);

            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function save()
    {
        $statement = $this->db->prepare('UPDATE employee SET
                                        firstname=:firstname,
                                        lastname=:lastname,
                                        email=:email,
                                        jobdescription=:jobdescription,
                                        datehired=:datehired,
                                        jobtype=:jobtype,
                                        jobstatus=:jobstatus,
                                        password=:password
                                        WHERE
                                        id=:id');

        try {
            $arr = [
                ':firstname' => $this->firstname,
                ':lastname'  => $this->lastname,
                ':email'     => $this->email,
                ':jobdescription' => $this->jobdescription,
                ':datehired' => $this->datehired,
                ':jobtype'   => $this->jobtype,
                ':jobstatus' => $this->jobstatus,
                ':password'  => $this->password,
                ':id'        => intval($this->id)
            ];
            $result = $statement->execute([
                ':firstname' => $this->firstname,
                ':lastname'  => $this->lastname,
                ':email'     => $this->email,
                ':jobdescription' => $this->jobdescription,
                ':datehired' => $this->datehired,
                ':jobtype'   => $this->jobtype,
                ':jobstatus' => $this->jobstatus,
                ':password'  => $this->password,
                ':id'        => intval($this->id)
            ]);

            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function delete($id)
    {
        $statement = $this->db->prepare('DELETE from employee where id=:id');
        try {
            $result = $statement->execute([
                ':id'     => $id
            ]);
            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function login($email)
    {
        $statement = $this->db->prepare('SELECT * from employee where email=:email');
        try {
            $result = $statement->execute([
                ':email'     => $email
            ]);

            if ($result === true) {
                $emp = $statement->fetch();
                if ($emp === false) return false;
                $this->populate($emp);
            }

            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getById($id)
    {
        $statement = $this->db->prepare('SELECT * from employee where id=:id');
        try {
            $result = $statement->execute([
                ':id'     => $id
            ]);

            if ($result === true) {
                $emp = $statement->fetch();
                if ($emp === false) return false;
                $this->populate($emp);
            }

            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function populate($data)
    {
        if(!empty($data['id'])) $this->id = $data['id'];

        $this->firstname = $data['firstname'];
        $this->lastname  = $data['lastname'];
        $this->email     = $data['email'];
        $this->jobdescription = $data['jobdescription'];
        $this->datehired = $data['datehired'];
        $this->jobtype   = $data['jobtype'];
        $this->jobstatus = $data['jobstatus'];
        $this->password  = $data['password'];
    }

    public function hashPassword($password)
    {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }

    public function jsonSerialize() {
        return array(
            'id'        => $this->id,
            'firstname' => $this->firstname,
            'lastname'  => $this->lastname,
            'email'     => $this->email,
            'jobdescription' => $this->jobdescription,
            'datehired' => $this->datehired,
            'jobtype'   => $this->jobtype,
            'jobstatus' => $this->jobstatus,
            'password'  => $this->password
        );
    }
}
