<?php

namespace Raw\Model;

use Raw\Model\BaseModel;
use Raw\Model\Employee;
use \PASSWORD_DEFAULT;

class Employees extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $statement = $this->db->prepare('SELECT * from employee');
        try {
            $result = $statement->execute();

            if ($result === true) {
                $emp_array = $statement->fetchAll();

                $list = array();
                for ($x=0; $x < count($emp_array); $x++) {
                  $employee = new Employee($emp_array[$x]);
                  array_push($list, $employee);
                }

                return $list;
            }

            return $result;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }
}
